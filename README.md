# Typwrtr

JavaScript plugin which provides typing effect to your website texts.

## Installation

1. Download latest release files.
2. Extract ZIP file.
3. Copy `typwrtr.js` and `typwrtr.css` (or `typwrtr.min.css`) to your project directory (by default, put CSS file in `css` folder, and JS file in `js` folder).
4. Add import tags to your HTML file:
   ```HTML
   <link rel="stylesheet" href="css/typwrtr.css">
   <script src="js/typwrtr.js"></script>
   ```
   I recommend you adding `<link>` tag between `<head>` tags, and `<script>` at the end of your HTML document.

## Usage

### Initialization

1. Create tag (`span` is recommended) with specific id:
   ```HTML
   <p>This text will not be changed, however this text will: <span class='specificId'></span></p>
   ```
   This tag will be used for initializing Typwrtr in specific text.
2. In your JS file or `<script>` tags create new Typwrtr object with specific ID of the tag where you want Typwrtr to work:
   ```HTML
   <script>
   new Typwrtr('specificId');
   </script>
   ```

### Options

Typwrtr is customizable through options. Options can be specified as parameter while creating Typwrtr object:
```HTML
<script>
new Typwrtr('specificId', {
    text: ['Good', 'Bad', 'Smart', 'Small', 'Proud'], 
    infinite: true,
    speed: 30 
    });
</script>
```

In this case, `text`, `infinite`, `speed` are options that you can modify. All of them (and much more) are specified down below. Options have to be placed in curly brackets a.k.a. `{}`.

#### Text

**Keyword:** `text`

**Input type**: array

**Default value:** `["It was a bright cold day in April, and the clocks were striking thirteen.", "It was a pleasure to burn."]`

**Description:** Represents the text that will be typed.

**Usage:**
- Single string:
```HTML
<script>
new Typwrtr('specificId', {
    text: ['This is the text that will be typed and typed all over again']
    });
</script>
```
- More strings:
```HTML
<script>
new Typwrtr('specificId', {
    text: ['This is the first sentence', 'This is the second sentence', 'This is the third sentence']
    });
</script>
```

*Fun fact: Default values are the introduction sentences of the books "1984" by George Orwell and "Fahrenheit 451" by Ray Bradbury.*

#### Typing Speed

**Keyword:** `speed`

**Input type**: integer

**Default value:** `50`

**Description:** Represents the typing/deleting speed in miliseconds.

**Usage:**

- set typing speed to 150 miliseconds:

```HTML
<script>
new Typwrtr('specificId', {
    speed: 150
    });
</script>
```

#### Pause Time 

**Keyword:** `pauseTime`

**Input type**: integer

**Default value:** `2500`

**Description:** Represents the pause time before starting the next typing sequence in miliseconds.

**Usage:**

- set pause time to 5 seconds:

```HTML
<script>
new Typwrtr('specificId', {
    pauseTime: 5000
    });
</script>
```

#### Infinite

**Keyword:** `infinite`

**Input type**: boolean

**Default value:** `true`

**Description:** Decides whether the typing will be infinte or it will happen only once.

**Usage:**

- set Typewriter to type only once:

```HTML
<script>
new Typwrtr('specificId', {
    infinite: false
    });
</script>
```

#### Text Cursor

**Keyword:** `textCursor`

**Input type**: boolean

**Default value:** `true`

**Description:** Show blinking cursor at the end of the string.

**Usage:**

- Turn off text cursor:

```HTML
<script>
new Typwrtr('specificId', {
    textCursor: false
    });
</script>
```

#### Backspace

**Keyword:** `backspace`

**Input type**: boolean

**Default value:** `true`

**Description:** Enable/disable character-by-character string removal (a.k.a. like pressing Backspace key periodically). The speed of removal is the same as typing speed.

**Usage:**

- Turn off Backspace:

```HTML
<script>
new Typwrtr('specificId', {
    backspace: false
    });
</script>
```

#### Fixed Width

**Keyword:** `width`

**Input type**: integer

**Default value:** `0`

**Description:** Use it to set up fixed width of Typwrtr in pixels. If it is set to `0`, the width will not be fixed.

**Usage:**

- set width to 300px:

```HTML
<script>
new Typwrtr('specificId', {
    width: 300
    });
</script>
```

### Styling

To add your own CSS styles, simply add your own ID or class to tag where you call Typwrtr.